#include "stdafx.h"
#include "Save16BitTIFFImage.h"

const int TIFFTAG_IMAGEWIDTH = 256;	/* image width in pixels */
const int TIFFTAG_IMAGELENGTH = 257;	/* image height in pixels */
const int TIFFTAG_BITSPERSAMPLE = 258;	/* bits per channel (sample) */
const int TIFFTAG_COMPRESSION = 259;	/* data compression technique */
const int COMPRESSION_NONE = 1;	/* dump mode */
const int COMPRESSION_CCITTRLE = 2;	/* CCITT modified Huffman RLE */
const int COMPRESSION_CCITTFAX3 = 3;	/* CCITT Group 3 fax encoding */
const int COMPRESSION_CCITTFAX4 = 4;	/* CCITT Group 4 fax encoding */
const int COMPRESSION_LZW = 5;	/* Lempel-Ziv  & Welch */
const int COMPRESSION_JPEG = 6;	/* !JPEG compression */
const int TIFFTAG_PHOTOMETRIC = 262;	/* photometric interpretation */
const int PHOTOMETRIC_MINISWHITE = 0;	/* min value is white */
const int PHOTOMETRIC_MINISBLACK = 1;	/* min value is black */
const int PHOTOMETRIC_RGB = 2;	/* RGB color model */
const int PHOTOMETRIC_PALETTE = 3;	/* color map indexed */
const int PHOTOMETRIC_MASK = 4;	/* $holdout mask */
const int PHOTOMETRIC_SEPARATED = 5;	/* !color separations */
const int PHOTOMETRIC_YCBCR = 6;	/* !CCIR 601 */
const int PHOTOMETRIC_CIELAB = 8;	/* !1976 CIE L*a*b* */
const int TIFFTAG_STRIPOFFSETS = 273;	/* offsets to data strips */
const int TIFFTAG_SAMPLESPERPIXEL = 277;	/* samples per pixel */
const int TIFFTAG_ROWSPERSTRIP = 278;	/* rows per strip of data */
const int TIFFTAG_STRIPBYTECOUNTS = 279;	/* bytes counts for strips */

typedef struct TIFF_FILE_tag
{
	UINT16 tagtype;
	UINT16 fieldtype;
	UINT32 numofvalue;
	UINT32 value;
} TIFFFILETAG;

CSave16BitTIFFImage::CSave16BitTIFFImage()
{

}

CSave16BitTIFFImage::~CSave16BitTIFFImage()
{

}

bool CSave16BitTIFFImage::SaveTIFFFile(CString filename, unsigned short **image, int width, int height)
{
	bool ret = false;
	CFile theFile;
	
	if (theFile.Open(filename, CFile::modeCreate | CFile::modeWrite))
	{
		char *software = "CellMax, Inc.";
		unsigned char header[4];
		header[0] = 'I';
		header[1] = 'I';
		header[2] = (BYTE)42;
		header[3] = (BYTE)0;
		theFile.Write(header, 4);
		UINT32 offset = width * height * 2 + 8;
		theFile.Write(&offset, 4);
		UINT32 *StripOffset = new UINT32[height];
		UINT32 *StripSize = new UINT32[height];
		offset = 8;
		for (int i = 0; i<height; i++, offset += (width * 2)) 
		{
			StripOffset[i] = offset;
			StripSize[i] = width * 2;
			theFile.Write(image[i], 2 * width);
		}

		/* The number of directory entries (12) */
		UINT16 numTags = 12;
		offset += (sizeof(TIFFFILETAG) * numTags + 2 + 4);
		theFile.Write(&numTags, 2); 
		TIFFFILETAG tag;
		memset(&tag, 0, sizeof(TIFFFILETAG));
		tag.tagtype = (UINT16)254; // (0)254
		tag.fieldtype = 4;
		tag.numofvalue = 1;
		tag.value = 0;
		theFile.Write(&tag, sizeof(TIFFFILETAG));
		memset(&tag, 0, sizeof(TIFFFILETAG));
		tag.tagtype = (UINT16)TIFFTAG_IMAGEWIDTH; // (1)256
		tag.fieldtype = 4;
		tag.numofvalue = 1;
		tag.value = width;
		theFile.Write(&tag, sizeof(TIFFFILETAG));
		memset(&tag, 0, sizeof(TIFFFILETAG));
		tag.tagtype = (UINT16)TIFFTAG_IMAGELENGTH; //(2)257
		tag.fieldtype = 4;
		tag.numofvalue = 1;
		tag.value = height;
		theFile.Write(&tag, sizeof(TIFFFILETAG));
		memset(&tag, 0, sizeof(TIFFFILETAG));
		tag.tagtype = (UINT16)TIFFTAG_BITSPERSAMPLE; //(3)258
		tag.fieldtype = 3;
		tag.numofvalue = 1;
		tag.value = 16;
		theFile.Write(&tag, sizeof(TIFFFILETAG));
		memset(&tag, 0, sizeof(TIFFFILETAG));
		tag.tagtype = (UINT16)TIFFTAG_COMPRESSION; //(4)259
		tag.fieldtype = 3;
		tag.numofvalue = 1;
		tag.value = 1;
		theFile.Write(&tag, sizeof(TIFFFILETAG));
		memset(&tag, 0, sizeof(TIFFFILETAG));
		tag.tagtype = (UINT16)TIFFTAG_PHOTOMETRIC; //(5)262
		tag.fieldtype = 3;
		tag.numofvalue = 1;
		tag.value = 1;
		theFile.Write(&tag, sizeof(TIFFFILETAG));
		memset(&tag, 0, sizeof(TIFFFILETAG));
		tag.tagtype = (UINT16)270; //(6) Image Description
		tag.fieldtype = 2;
		tag.numofvalue = 1;
		tag.value = 0;
		theFile.Write(&tag, sizeof(TIFFFILETAG));
		memset(&tag, 0, sizeof(TIFFFILETAG));
		tag.tagtype = (UINT16)TIFFTAG_STRIPOFFSETS; // (7) 273
		tag.fieldtype = 4;
		tag.numofvalue = height;
		tag.value = offset;
		theFile.Write(&tag, sizeof(TIFFFILETAG));
		memset(&tag, 0, sizeof(TIFFFILETAG));
		tag.tagtype = (UINT16)TIFFTAG_SAMPLESPERPIXEL; //(8)277
		tag.fieldtype = 3;
		tag.numofvalue = 1;
		tag.value = 1;
		theFile.Write(&tag, sizeof(TIFFFILETAG));
		memset(&tag, 0, sizeof(TIFFFILETAG));
		tag.tagtype = (UINT16)TIFFTAG_ROWSPERSTRIP; //(9)278
		tag.fieldtype = 3;
		tag.numofvalue = 1;
		tag.value = 1;
		theFile.Write(&tag, sizeof(TIFFFILETAG));
		memset(&tag, 0, sizeof(TIFFFILETAG));
		tag.tagtype = (UINT16)TIFFTAG_STRIPBYTECOUNTS; // (10) 279
		tag.fieldtype = 4;
		tag.numofvalue = height;
		tag.value = offset + height * sizeof(UINT32);
		theFile.Write(&tag, sizeof(TIFFFILETAG));
		memset(&tag, 0, sizeof(TIFFFILETAG));
		tag.tagtype = (UINT16)305; // (11) 305
		tag.fieldtype = 2;
		tag.numofvalue = (UINT32) strlen(software);
		tag.value = offset + 2 * (height * sizeof(UINT32));
		theFile.Write(&tag, sizeof(TIFFFILETAG));
		UINT32 terminateIFD = 0;
		theFile.Write(&terminateIFD, sizeof(UINT32));
		theFile.Write(StripOffset, sizeof(UINT32) * height);
		theFile.Write(StripSize, sizeof(UINT32) * height);
		theFile.Write(software, (UINT32)strlen(software));
		theFile.Close();
		delete[] StripOffset;
		delete[] StripSize;
		ret = true;
	}
	return ret;
}