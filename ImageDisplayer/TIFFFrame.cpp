#include "stdafx.h"
#include "TIFFFrame.h"
#include <string.h>

using namespace std;

const int TIFFTAG_IMAGEWIDTH = 256;	/* image width in pixels */
const int TIFFTAG_IMAGELENGTH = 257;	/* image height in pixels */
const int TIFFTAG_BITSPERSAMPLE = 258;	/* bits per channel (sample) */
const int TIFFTAG_COMPRESSION = 259;	/* data compression technique */
const int COMPRESSION_NONE = 1;	/* dump mode */
const int COMPRESSION_CCITTRLE = 2;	/* CCITT modified Huffman RLE */
const int COMPRESSION_CCITTFAX3 = 3;	/* CCITT Group 3 fax encoding */
const int COMPRESSION_CCITTFAX4 = 4;	/* CCITT Group 4 fax encoding */
const int COMPRESSION_LZW = 5;	/* Lempel-Ziv  & Welch */
const int COMPRESSION_JPEG = 6;	/* !JPEG compression */
const int TIFFTAG_PHOTOMETRIC = 262;	/* photometric interpretation */
const int PHOTOMETRIC_MINISWHITE = 0;	/* min value is white */
const int PHOTOMETRIC_MINISBLACK = 1;	/* min value is black */
const int PHOTOMETRIC_RGB = 2;	/* RGB color model */
const int PHOTOMETRIC_PALETTE = 3;	/* color map indexed */
const int PHOTOMETRIC_MASK = 4;	/* $holdout mask */
const int PHOTOMETRIC_SEPARATED = 5;	/* !color separations */
const int PHOTOMETRIC_YCBCR = 6;	/* !CCIR 601 */
const int PHOTOMETRIC_CIELAB = 8;	/* !1976 CIE L*a*b* */
const int TIFFTAG_STRIPOFFSETS = 273;	/* offsets to data strips */
const int TIFFTAG_SAMPLESPERPIXEL = 277;	/* samples per pixel */
const int TIFFTAG_ROWSPERSTRIP = 278;	/* rows per strip of data */
const int TIFFTAG_STRIPBYTECOUNTS = 279;	/* bytes counts for strips */

CTIFFFrame::CTIFFFrame()
{
	m_Width = 0;
	m_Height = 0;
	m_Image = NULL;
}

CTIFFFrame::~CTIFFFrame()
{
	if (m_Image != NULL)
		delete m_Image;
}

BOOL CTIFFFrame::LoadRawTIFFFile(CString filename)
{
	BOOL ret = FALSE;
	CFile theFile;
	CString message;
	UINT count;
	UINT32 *ptr;
	int len;

	if (m_Image != NULL)
	{
		delete m_Image;
		m_Image = NULL;
		m_Width = m_Height = 0;
	}
	if (theFile.Open(filename, CFile::modeRead))
	{
		BYTE *buf = NULL;
		UINT32 *StripOffsetArray = NULL;
		UINT32 *StripByteCountArray = NULL;
		while (TRUE)
		{
			ULONGLONG numBytes = theFile.GetLength();
			if (numBytes < 8)
			{
				message.Format(_T("theFile.GetLength() < 8"));
				AfxMessageBox(message);
				break;
			}
			buf = new BYTE[8];
			count = theFile.Read(buf, 8);
			if (count != 8)
			{
				message.Format(_T("Failed to read 8 byte TIFF Header (BytesRead=%d)"), count);
				AfxMessageBox(message);
				break;
			}
			if ((buf[0] != buf[1]) || (buf[0] != 'I'))
			{
				message.Format(_T("This TIFFFileReader only supports Little Indian TIFF File"));
				AfxMessageBox(message);
				break;
			}
			ptr = (UINT32*)&buf[4];
			if (*ptr >= numBytes)
			{
				message.Format(_T("Tag Offset(=%lu) >= File Size(=%lu)"), *ptr, numBytes);
				AfxMessageBox(message);
				break;
			}
			theFile.Seek(*ptr, CFile::begin);
			len = (int)(numBytes - *ptr);
			delete buf;
			buf = new BYTE[len];
			count = theFile.Read(buf, len);
			if (count != len)
			{
				message.Format(_T("Tag Length(=%d) != DataRead (=%d)"), len, count);
				AfxMessageBox(message);
				break;
			}

			UINT16 numTags = *((UINT16*)&buf[0]);
			UINT32 offset = 2;
			UINT16 tag;
			UINT32 CountForStripOffsetArray = 0;
			UINT32 CountForStripByteCountArray = 0;
			UINT32 OffsetToStripOffsetArray = 0;
			UINT32 OffsetToStripByteCountArray = 0;

			for (int i = 0; i < numTags; i++, offset += 12)
			{
				tag = *((UINT16 *)&buf[offset]);
				if (tag == TIFFTAG_IMAGEWIDTH)
					m_Width = *((UINT32 *)&buf[offset + 8]);
				else if (tag == TIFFTAG_IMAGELENGTH)
					m_Height = *((UINT32 *)&buf[offset + 8]);
				else if (tag == TIFFTAG_STRIPOFFSETS)
				{
					CountForStripOffsetArray = *((UINT32 *)&buf[offset + 4]);
					OffsetToStripOffsetArray = *((UINT32 *)&buf[offset + 8]);
				}
				else if (tag == TIFFTAG_STRIPBYTECOUNTS)
				{
					CountForStripByteCountArray = *((UINT32 *)&buf[offset + 4]);
					OffsetToStripByteCountArray = *((UINT32 *)&buf[offset + 8]);
				}
			}

			StripOffsetArray = new UINT32[CountForStripOffsetArray];
			theFile.Seek(OffsetToStripOffsetArray, CFile::begin);
			len = (int)(4 * CountForStripOffsetArray);
			delete buf;
			buf = new BYTE[len];
			count = theFile.Read(buf, len);
			if (count != len)
			{
				message.Format(_T("StripOffsetArray Length(=%d) != DataRead (=%d)"), len, count);
				AfxMessageBox(message);
				break;
			}
			
			offset = 0;
			for (int j = 0; j < (int)CountForStripOffsetArray; j++, offset += 4)
			{
				StripOffsetArray[j] = *((UINT32 *)&buf[offset]);
			}
			
			StripByteCountArray = new UINT32[CountForStripByteCountArray];
			theFile.Seek(OffsetToStripByteCountArray, CFile::begin);
			len = (int)(4 * CountForStripByteCountArray);
			delete buf;
			buf = new BYTE[len];
			count = theFile.Read(buf, len);
			if (count != len)
			{
				message.Format(_T("StripByteCountArray Length(=%d) != DataRead (=%d)"), len, count);
				AfxMessageBox(message);
				break;
			}

			offset = 0;
			for (int j = 0; j < (int)CountForStripByteCountArray; j++, offset += 4)
			{
				StripByteCountArray[j] = *((UINT32 *)&buf[offset]);
			}

			int rowIndex = 0;
			int columnIndex = 0;
			int numPixels;
			BOOL exitWhileLoop = FALSE;
			m_Image = new unsigned short[m_Width * m_Height];
			for (int i = 0; i < (int)CountForStripOffsetArray; i++)
			{
				theFile.Seek(StripOffsetArray[i], CFile::begin);
				len = (int)StripByteCountArray[i];
				delete buf;
				buf = new BYTE[len];
				count = theFile.Read(buf, len);
				if (count != len)
				{
					message.Format(_T("ImageData Length(=%d) != DataRead (=%d)"), len, count);
					AfxMessageBox(message);
					exitWhileLoop = TRUE;
					break;
				}

				offset = 0;
				numPixels = (int)(len / 2);
				for (int j = 0; j < numPixels; j++, offset += 2)
				{
					unsigned short value = *((unsigned short *)&buf[offset]);
					m_Image[rowIndex * m_Width + columnIndex] = value;
					columnIndex++;
					if (columnIndex == m_Width)
					{
						columnIndex = 0;
						rowIndex++;
					}
				}
			}
			
			if (exitWhileLoop)
				break;

			ret = TRUE;
			// quit while loop
			break;
		}
		
		theFile.Close();
		if (StripOffsetArray != NULL)
			delete StripOffsetArray;
		if (StripByteCountArray != NULL)
			delete StripByteCountArray;
		delete buf;
	}

	return ret;
}