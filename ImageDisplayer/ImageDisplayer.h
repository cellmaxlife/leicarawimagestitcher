
#pragma once
#include <afxwin.h>
#include "resource.h"
#include <vector>

using namespace std;

typedef enum Display_Enum
{
	SINGLE_FRAME,
	TWO_VERTICAL_FRAME,
	TWO_HORIZONTAL_FRAME
} DISPLAYENUM;

class ImageDisplayerApp : public CWinApp
{
public:
	// override InitInstance to do our initialisation
	virtual BOOL InitInstance();
};

class ImageDisplayerWnd : public CFrameWnd
{
public:
	// constructor to create the window
	ImageDisplayerWnd();
	
protected:
	BYTE GetContrastEnhancedByte(unsigned short value, int contrast);
	// declare our handlers
	afx_msg void OnPaint();
	afx_msg int OnCreate(LPCREATESTRUCT);
	afx_msg void OnClose();
	afx_msg void OnSize(UINT, int, int);
	// this declares the message map and should be
	// the last class member
	DECLARE_MESSAGE_MAP()
	CDC m_dcMem;
	bool LoadScanFile(CString filename);
	CMenu m_Menu;
	void OpenScanFile();

public:
	CImage m_Image;
	int m_NumFrames;
	int m_NumRows;
	int m_NumColumns;
	CString m_ScanFilename;
	int m_ImageWidth, m_ImageHeight;
	unsigned short **m_RedFrames;
	unsigned short **m_GreenFrames;
	unsigned short **m_BlueFrames;
	void FreeFrameData();
	void Log(CString message, CString logfilename);
	void DisplayTwoFrameVertically(int topIndex, int bottomIndex, int verticalOverlappedPixels);
	void DisplayTwoFrameHorizontally(int leftIndex, int rightIndex, int horizontalOverlappedPixels);
	DISPLAYENUM m_DisplayFlag;
	CString m_Caption;
	void SetStatus(CString message);
	int FindVerticalOverlappedPixels(int topIndex, int bottomIndex);
	int FindHorizontalOverlappedPixels(int leftIndex, int rightIndex);
};

