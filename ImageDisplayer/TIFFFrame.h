#pragma once
#include "afxwin.h"

class CTIFFFrame
{
public:
	CTIFFFrame();
	virtual ~CTIFFFrame();

public:
	int m_Width;
	int m_Height;
	unsigned short *m_Image;
	BOOL LoadRawTIFFFile(CString filename);
};
