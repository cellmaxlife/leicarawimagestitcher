#pragma once
#include <stack>
#include <vector>
#include "BlobData.h"

using namespace std;

class CFindConnectedPixels
{
protected:
	int m_Width;
	int m_Height;
	unsigned short *m_Image;
	stack <int> m_Stack;
	unsigned short m_Threshold;
	unsigned short *m_Map;
	void DoLabeling(CBlobData *blob);
	void CleanUp();

public:
	CFindConnectedPixels();
	virtual ~CFindConnectedPixels();

	void FindConnectedPixels(unsigned short *image, int width, int height, int threshold, vector<CBlobData *> *blobList);
	void FindConnectedPixels(CBlobData *blob, int threshold, vector<CBlobData *> *blobList);
};

