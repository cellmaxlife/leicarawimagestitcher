#include "stdafx.h"
#include "ImageDisplayer.h"
#include "TIFFFrame.h"
#include "Save16BitTIFFImage.h"
#include <thread>
#include "FindConnectedPixels.h"

using namespace std;

#define MAX_CONTRAST	5500

#define TIFF_FRAME_WIDTH	1344
#define TIFF_FRAME_HEIGHT	1024

#define MAX_SCREEN_WIDTH	2400
#define MAX_SCREEN_HEIGHT	1600

#define ID_OPEN_FILE1	40001

static UINT screentype;
static int screenx;
static int screeny;

static DWORD WINAPI StitchingOperation(LPVOID param);

// our application object. must be global
ImageDisplayerApp theApp;
// The usual initialisation
BOOL ImageDisplayerApp::InitInstance()
{
	m_pMainWnd = new ImageDisplayerWnd();
	m_nCmdShow = SW_SHOWMAXIMIZED;
	m_pMainWnd->ShowWindow(m_nCmdShow);
	m_pMainWnd->UpdateWindow();
	return TRUE;
}
// The message map
BEGIN_MESSAGE_MAP(ImageDisplayerWnd, CFrameWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_PAINT()
	ON_WM_CLOSE()
	ON_COMMAND(ID_OPEN_FILE1, OpenScanFile)
END_MESSAGE_MAP()

// constructor to create the window
ImageDisplayerWnd::ImageDisplayerWnd()
{
	m_NumFrames = 0;
	m_NumRows = 0;
	m_NumColumns = 0;
	m_RedFrames = NULL;
	m_GreenFrames = NULL;
	m_BlueFrames = NULL;
	Create(NULL, _T("LeicaRawImageStitcher(V1)"), WS_OVERLAPPEDWINDOW);
}

void ImageDisplayerWnd::FreeFrameData()
{
	for (int i = 0; i < m_NumFrames; i++)
	{
		if (m_RedFrames[i] != NULL)
		{
			delete[] m_RedFrames[i];
			m_RedFrames[i] = NULL;
		}
		if (m_GreenFrames[i] != NULL)
		{
			delete[] m_GreenFrames[i];
			m_GreenFrames[i] = NULL;
		}
		if (m_BlueFrames[i] != NULL)
		{
			delete[] m_BlueFrames[i];
			m_BlueFrames[i] = NULL;
		}
	}
	delete[] m_RedFrames;
	delete[] m_GreenFrames;
	delete[] m_BlueFrames;
}

void ImageDisplayerWnd::OnClose()
{
	FreeFrameData();
	CFrameWnd::OnClose();
}

// this gets called on a WM_CREATE
int ImageDisplayerWnd::OnCreate(LPCREATESTRUCT createstruct)
{
	// always call the base class OnCreate to give the FrameWnd
	// chance to initialise
	if (CFrameWnd::OnCreate(createstruct) == -1) return -1;
	m_Caption = _T("LeicaRawImageStitcher(V1)");
	m_Menu.LoadMenu(IDR_MENU1);
	SetMenu(&m_Menu);

	// get a client area DC for this window
	CClientDC dc(this);
	m_dcMem.CreateCompatibleDC(&dc);
	CImage image;
	image.Create(MAX_SCREEN_WIDTH, -MAX_SCREEN_HEIGHT, 24);
	BYTE *pCursor = (BYTE *)image.GetBits();
	memset(pCursor, 0, sizeof(BYTE) * image.GetPitch() * MAX_SCREEN_HEIGHT);
	CBitmap bitmap;
	bitmap.Attach(image.Detach());
	CBitmap *pOldMap = m_dcMem.SelectObject(&bitmap);
	if (pOldMap != NULL)
		pOldMap->DeleteObject();
	if (m_Image != NULL)
		m_Image.Destroy();
	m_Image.Create(TIFF_FRAME_WIDTH, -TIFF_FRAME_HEIGHT, 24);
	memset((BYTE *)m_Image.GetBits(), 0, sizeof(BYTE) * m_Image.GetPitch() * TIFF_FRAME_HEIGHT);
	m_DisplayFlag = SINGLE_FRAME;
	// return 0 if all went well
	return 0;
}

// this gets called on WM_SIZE
void ImageDisplayerWnd::OnSize(UINT type, int x, int y)
{
	// call base class OnSize
	CFrameWnd::OnSize(type, x, y);
	screentype = type;
	screenx = x;
	screeny = y;
}

// This gets called on a WM_PAINT
void ImageDisplayerWnd::OnPaint()
{
	// get a painting DC
	CPaintDC dc(this);
	CRect rect;
	GetClientRect(&rect);
	dc.SetStretchBltMode(HALFTONE);
	m_Image.StretchBlt(m_dcMem, rect);
	dc.BitBlt(rect.left, rect.top, rect.Width(), rect.Height(), &m_dcMem, 0, 0, SRCCOPY);
	if (m_DisplayFlag == TWO_VERTICAL_FRAME)
	{
		CPen CursorPen(PS_SOLID, 3, RGB(255, 255, 255));
		CPen *pOldPen = dc.SelectObject(&CursorPen);
		dc.MoveTo(rect.left + rect.Width() / 2, rect.top);
		dc.LineTo(rect.left + rect.Width() / 2, rect.bottom);
		dc.MoveTo(rect.left, rect.top + rect.Height() / 2);
		dc.LineTo(rect.left + rect.Width() / 2, rect.top + rect.Height() / 2);
	}
	else if (m_DisplayFlag == TWO_HORIZONTAL_FRAME)
	{
		CPen CursorPen(PS_SOLID, 3, RGB(255, 255, 255));
		CPen *pOldPen = dc.SelectObject(&CursorPen);
		dc.MoveTo(rect.left, rect.top + rect.Height() / 2);
		dc.LineTo(rect.right, rect.top + rect.Height() / 2);
		dc.MoveTo(rect.left + rect.Width() / 2, rect.top);
		dc.LineTo(rect.left + rect.Width() / 2, rect.top + rect.Height() / 2);
	}
}

BYTE ImageDisplayerWnd::GetContrastEnhancedByte(unsigned short value, int contrast)
{
	BYTE result = 0;
	int value1 = value;
	int maxValue = contrast;
	if (maxValue < 255)
		maxValue = 255;
	if (value1 < 0)
		result = (BYTE)0;
	else if (value1 > maxValue)
		result = (BYTE)255;
	else
	{
		result = (BYTE)(255.0 * value1 / maxValue);
	}
	return result;
}

bool ImageDisplayerWnd::LoadScanFile(CString filename)
{
	bool ret = false;
	CStdioFile theFile;
	if (theFile.Open(filename, CFile::modeRead | CFile::typeText))
	{
		CString textline;
		CString previousLine;
		while (theFile.ReadString(textline))
		{
			if (textline.Find(_T("Overview")) > -1)
			{
				CString subStr = _T("Stage");
				int index = textline.Find(subStr);
				textline = textline.Mid(index + subStr.GetLength());
				index = textline.Find(_T(","));
				textline = textline.Mid(0, index - 1);
				m_NumFrames = _wtoi(textline) - 1;
				subStr = _T("Row");
				index = previousLine.Find(subStr);
				previousLine = previousLine.Mid(index + subStr.GetLength());
				index = previousLine.Find(_T("_"));
				textline = previousLine.Mid(0, index);
				m_NumRows = _wtoi(textline) + 1;
				subStr = _T("Col");
				index = previousLine.Find(subStr);
				previousLine = previousLine.Mid(index + subStr.GetLength());
				index = previousLine.Find(_T("\""));
				textline = previousLine.Mid(0, index);
				m_NumColumns = _wtoi(textline) + 1;
				ret = true;
				break;
			}
			else
				previousLine = textline;
		}
		theFile.Close();
	}
	return ret;
}

void ImageDisplayerWnd::OpenScanFile()
{
	FreeFrameData();
	CFileDialog dlg(TRUE,    // open
		NULL,    // no default extension
		NULL,    // no initial file name
		OFN_FILEMUSTEXIST
		| OFN_HIDEREADONLY,
		_T("Leica SCAN files (*.SCAN)|*.SCAN"), NULL, 0, TRUE);
	if (dlg.DoModal() == IDOK)
	{
		m_ScanFilename = dlg.GetPathName();
		if (!LoadScanFile(m_ScanFilename))
		{
			AfxMessageBox(_T("Failed to load SCAN File"));
		}
		else
		{
			HANDLE thread = ::CreateThread(NULL, 0, StitchingOperation, this, CREATE_SUSPENDED, NULL);
			if (!thread)
			{
				AfxMessageBox(_T("Fail to create thread for doing stitching"));
				return;
			}
			::SetThreadPriority(thread, THREAD_PRIORITY_NORMAL);
			::ResumeThread(thread);
			SetStatus(m_ScanFilename);
		}
	}
}

void ImageDisplayerWnd::SetStatus(CString message)
{
	CString caption;
	caption.Format(_T("%s - %s"), m_Caption, message);
	SetWindowText(caption);
}

void ImageDisplayerWnd::DisplayTwoFrameVertically(int topIndex, int bottomIndex, int verticalOverlappedPixels)
{
	m_DisplayFlag = TWO_VERTICAL_FRAME;
	if (m_Image != NULL)
		m_Image.Destroy();
	int searchheight = (int)(0.25 * TIFF_FRAME_HEIGHT);
	m_Image.Create(2 * TIFF_FRAME_WIDTH, -2 * searchheight, 24);
	memset(m_Image.GetBits(), 0, sizeof(BYTE) * m_Image.GetPitch() * 2 * searchheight);
	int topstart = TIFF_FRAME_HEIGHT - searchheight;
	unsigned short *red = m_RedFrames[topIndex] + topstart * TIFF_FRAME_WIDTH;
	unsigned short *green = m_GreenFrames[topIndex] + topstart * TIFF_FRAME_WIDTH;
	unsigned short *blue = m_BlueFrames[topIndex] + topstart * TIFF_FRAME_WIDTH;
	int stride = m_Image.GetPitch() - 3 * TIFF_FRAME_WIDTH;
	BYTE *pCursor = (BYTE *)m_Image.GetBits();
	for (int i = topstart; i < TIFF_FRAME_HEIGHT; i++)
	{
		for (int j = 0; j < TIFF_FRAME_WIDTH; j++, red++, green++, blue++)
		{
			*pCursor++ = GetContrastEnhancedByte(*blue, MAX_CONTRAST);
			*pCursor++ = GetContrastEnhancedByte(*green, MAX_CONTRAST);
			*pCursor++ = GetContrastEnhancedByte(*red, MAX_CONTRAST);
		}
		if (stride > 0)
			pCursor += stride;
	}
	red = m_RedFrames[bottomIndex];
	green = m_GreenFrames[bottomIndex];
	blue = m_BlueFrames[bottomIndex];
	for (int i = 0; i < searchheight; i++)
	{
		for (int j = 0; j < TIFF_FRAME_WIDTH; j++, red++, green++, blue++)
		{
			*pCursor++ = GetContrastEnhancedByte(*blue, MAX_CONTRAST);
			*pCursor++ = GetContrastEnhancedByte(*green, MAX_CONTRAST);
			*pCursor++ = GetContrastEnhancedByte(*red, MAX_CONTRAST);
		}
		if (stride > 0)
			pCursor += stride;
	}
	Invalidate();
}

void ImageDisplayerWnd::DisplayTwoFrameHorizontally(int leftIndex, int rightIndex, int horizontalOverlappedPixels)
{
	m_DisplayFlag = TWO_HORIZONTAL_FRAME;
	if (m_Image != NULL)
		m_Image.Destroy();
	m_Image.Create(2 * TIFF_FRAME_WIDTH, -2 * TIFF_FRAME_HEIGHT, 24);
	memset(m_Image.GetBits(), 0, sizeof(BYTE) * m_Image.GetPitch() * 2 * TIFF_FRAME_HEIGHT);
	unsigned short *redLeft = m_RedFrames[leftIndex];
	unsigned short *greenLeft = m_GreenFrames[leftIndex];
	unsigned short *blueLeft = m_BlueFrames[leftIndex];
	unsigned short *redRight = m_RedFrames[rightIndex];
	unsigned short *greenRight = m_GreenFrames[rightIndex];
	unsigned short *blueRight = m_BlueFrames[rightIndex];
	int stride = m_Image.GetPitch() - 6 * TIFF_FRAME_WIDTH;
	BYTE *pCursor = (BYTE *)m_Image.GetBits();
	for (int i = 0; i < TIFF_FRAME_HEIGHT; i++)
	{
		for (int j = 0; j < TIFF_FRAME_WIDTH; j++, redLeft++, greenLeft++, blueLeft++)
		{
			*pCursor++ = GetContrastEnhancedByte(*blueLeft, MAX_CONTRAST);
			*pCursor++ = GetContrastEnhancedByte(*greenLeft, MAX_CONTRAST);
			*pCursor++ = GetContrastEnhancedByte(*redLeft, MAX_CONTRAST);
		}
		for (int j = 0; j < TIFF_FRAME_WIDTH; j++, redRight++, greenRight++, blueRight++)
		{
			*pCursor++ = GetContrastEnhancedByte(*blueRight, MAX_CONTRAST);
			*pCursor++ = GetContrastEnhancedByte(*greenRight, MAX_CONTRAST);
			*pCursor++ = GetContrastEnhancedByte(*redRight, MAX_CONTRAST);
		}
		if (stride > 0)
			pCursor += stride;
	}
	Invalidate();
}

DWORD WINAPI StitchingOperation(LPVOID param)
{
	clock_t time1 = clock();
	ImageDisplayerWnd *ptr = (ImageDisplayerWnd *)param;
	CString textline;
	CStdioFile theFile;
	CString LogFileName = _T("C:\\CTCReviewerLog\\StitchImageLog.txt");
	if (theFile.Open(LogFileName, CFile::modeCreate | CFile::modeWrite | CFile::typeText))
	{
		textline.Format(_T("ScanFilename=%s\n"), ptr->m_ScanFilename);
		theFile.WriteString(textline);
		textline.Format(_T("NumFrames=%d,NumRows=%d,NumColumns=%d\n"),
			ptr->m_NumFrames, ptr->m_NumRows, ptr->m_NumColumns);
		theFile.WriteString(textline);
		theFile.Close();
	}
	ptr->m_RedFrames = new unsigned short *[ptr->m_NumFrames];
	ptr->m_GreenFrames = new unsigned short *[ptr->m_NumFrames];
	ptr->m_BlueFrames = new unsigned short *[ptr->m_NumFrames];
	for (int i = 0; i < ptr->m_NumFrames; i++)
	{
		ptr->m_RedFrames[i] = NULL;
		ptr->m_GreenFrames[i] = NULL;
		ptr->m_BlueFrames[i] = NULL;
	}
	CTIFFFrame frameReader;
	CString filename = ptr->m_ScanFilename.Mid(0, ptr->m_ScanFilename.Find(_T(".scan")));
	//for (int k = 0; k < ptr->m_NumFrames; k++)
	for (int k = 0; k < ptr->m_NumRows; k++)
	{
		CString filename1;
		filename1.Format(_T("%s_w1_s%d_t1.TIF"), filename, k + 1);
		BOOL status = frameReader.LoadRawTIFFFile(filename1);
		if (!status)
		{
			textline.Format(_T("Failed to load %s\n"), filename1);
			ptr->Log(textline, LogFileName);
			break;
		}
		if ((frameReader.m_Width != TIFF_FRAME_WIDTH) || (frameReader.m_Height != TIFF_FRAME_HEIGHT))
		{
			textline.Format(_T("TIFF Frame Width(=%d) != %d, or TIFF Frame Height(=%d) != %d\n"), frameReader.m_Width, TIFF_FRAME_WIDTH,
				frameReader.m_Height, TIFF_FRAME_HEIGHT);
			ptr->Log(textline, LogFileName);
			break;
		}
		ptr->m_RedFrames[k] = new unsigned short[TIFF_FRAME_WIDTH * TIFF_FRAME_HEIGHT];
		memcpy(ptr->m_RedFrames[k], frameReader.m_Image, sizeof(unsigned short) * TIFF_FRAME_WIDTH * TIFF_FRAME_HEIGHT);
		filename1.Format(_T("%s_w2_s%d_t1.TIF"), filename, k + 1);
		status = frameReader.LoadRawTIFFFile(filename1);
		if (!status)
		{
			textline.Format(_T("Failed to load %s\n"), filename1);
			ptr->Log(textline, LogFileName);
			break;
		}
		ptr->m_GreenFrames[k] = new unsigned short[TIFF_FRAME_WIDTH * TIFF_FRAME_HEIGHT];
		memcpy(ptr->m_GreenFrames[k], frameReader.m_Image, sizeof(unsigned short) * TIFF_FRAME_WIDTH * TIFF_FRAME_HEIGHT);
		filename1.Format(_T("%s_w3_s%d_t1.TIF"), filename, k + 1);
		status = frameReader.LoadRawTIFFFile(filename1);
		if (!status)
		{
			textline.Format(_T("Failed to load %s\n"), filename1);
			ptr->Log(textline, LogFileName);
			break;
		}
		ptr->m_BlueFrames[k] = new unsigned short[TIFF_FRAME_WIDTH * TIFF_FRAME_HEIGHT];
		memcpy(ptr->m_BlueFrames[k], frameReader.m_Image, sizeof(unsigned short) * TIFF_FRAME_WIDTH * TIFF_FRAME_HEIGHT);
	}
	
	int leftMiddleRowIndex = ptr->m_NumRows / 2;
	int rightMiddleRowIndex = ptr->m_NumFrames - leftMiddleRowIndex;
	int topMiddleColumnIndex = ptr->m_NumRows * (ptr->m_NumColumns / 2);
	int bottomMiddleColumnIndex = topMiddleColumnIndex + (ptr->m_NumRows - 1);
	textline.Format(_T("Left=%d,Right=%d,Top=%d,Bottom=%d"), leftMiddleRowIndex, rightMiddleRowIndex, topMiddleColumnIndex, bottomMiddleColumnIndex);
	ptr->Log(textline, LogFileName);
	int leftVerticalOverlappedPixels = ptr->FindVerticalOverlappedPixels(leftMiddleRowIndex, leftMiddleRowIndex + 1);
	ptr->DisplayTwoFrameVertically(leftMiddleRowIndex, leftMiddleRowIndex + 1, leftVerticalOverlappedPixels);
	clock_t time2 = clock();
	clock_t timediff = time2 - time1;
	float elapsedTime = ((float)timediff) / CLOCKS_PER_SEC;
	textline.Format(_T("Elapsed Time=%.2f (sec)\n"), elapsedTime);
	ptr->Log(textline, LogFileName);
	textline.Format(_T("Elapsed Time=%.2f (sec)"), elapsedTime);
	ptr->SetStatus(textline);
	return 0;
}

void ImageDisplayerWnd::Log(CString message, CString logfilename)
{
	CStdioFile theFile;
	if (theFile.Open(logfilename, CFile::modeNoTruncate | CFile::modeWrite | CFile::typeText))
	{
		theFile.SeekToEnd();
		theFile.WriteString(message);
		theFile.Close();
	}
}

int ImageDisplayerWnd::FindVerticalOverlappedPixels(int topIndex, int bottomIndex)
{
	return 0;
}

int ImageDisplayerWnd::FindHorizontalOverlappedPixels(int leftIndex, int rightIndex)
{
	return 0;
}
