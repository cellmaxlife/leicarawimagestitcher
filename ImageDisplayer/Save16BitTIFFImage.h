#pragma once
#include <afxwin.h>

class CSave16BitTIFFImage
{
public:
	CSave16BitTIFFImage();
	~CSave16BitTIFFImage();

	bool SaveTIFFFile(CString filename, unsigned short **image, int width, int height);
};